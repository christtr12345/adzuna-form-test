//capture all required elements from the form
const form = document.getElementById("form");
const fullName = document.getElementById("name");
const email = document.getElementById("email");
const notes = document.getElementById("notes");
const thankyou = document.getElementById("thank-you");

//set flags to false. Will be used to check
//if all the fields are valid to hide the form
//and display the thank you message
let nameValid = false;
let emailValid = false;
let notesValid = false;

//initialise the count variable globally so it can
//access below and for checking if the word count
//is over 20 in validateInput()
let count;

//listening for input on the textarea element.
//function is called on input
notes.addEventListener("input", () => {
    
    //get the input text value
    let words = notes.value;

    //initialise the word count
    count = 0;

    // split the words on each
    //space character
    let split = words.split(' ');

    //Loop through the words and increase
    //the counter when each split word
    //is not empty
    for (var i = 0; i < split.length; i++){
        if(split[i] != ""){
            count++;
        }
    }
    //display it in the output by setting the text content
    //of count result p tag to equal the count
    document.getElementById("count-result").
    textContent = `Total words: ${count}`;
});

//function to check each field has a valid input
function validateInput(){

    //trim the full name value to remove any whitespace
    //and check if it is not empty
    if(fullName.value.trim() === ""){
        //if empty, call the onError function and
        //inform the user of the error by passing the 
        //'full name required' argument
        onError(fullName, "Full Name required");
    
        //otherwise, successful input and call the onSuccess function
    } else {
        onSuccess(fullName);
        //set flag to true. this will determine if the form can be
        //hidden and the thank you message can appear
        nameValid = true;
    }
    //trim the email value to remove any whitespace
    //and check if it is not empty  
    if(email.value.trim() === ""){
        //if empty, call the onError function and
        //inform the user of the error by passing the 
        //'email cannot be empty' argument
        onError(email, "Email cannot be empty");

        //then check if them email format is valid 
    } else{
        //if the isValidEmail function return a failure
        if(!isValidEmail(email.value.trim())){
            //alert the user the input isn't valid by calling
            //the onError function, passing the 'email format not valid'
            //argument
            onError(email, "Email format not valid");

            //otherwsie the email input is valid
            //call the onSuccess function
        } else {
            onSuccess(email);
            //set flag to true. this will determine if the form can be
            //hidden and the thank you message can appear
            emailValid = true;
        }
    }

    //word count must be 20 words or more. Use the count variable to check this
    if(count < 20 || notes.value.trim() == ""){
        //if not, alert the user they must enter more than 20 words.
        onError(notes, "Must be 20 words or more!");

        //otherwise, notes entry is valid
    } else {
        //call the onSuccess function, passing in the notes input
        onSuccess(notes);
        //set flag to true. this will determine if the form can be
        //hidden and the thank you message can appear
        notesValid = true;
    }

    //if all the flags are true, meaning all inputs are valid...
    if(nameValid && emailValid && notesValid){
        //add the hide classlist to hide the form
        form.classList.add("hide");
        //set the inner text to thank you
        thankyou.innerText = "Thank you!";
        //align the text to the centre.
        thankyou.style.textAlign = "center";
    }
}

//this is listening for a click on the button
document.querySelector("button").addEventListener("click", (event)=>{
    //prevent the browswer from executing the default action.
    event.preventDefault();
    //call the validate inputs function to check the inputs the user has entered
    validateInput();
});

/*helper function to assign success features
to input argument.
input will be name, email or notes*/
function onSuccess(input){
    //target the parent element and store as variable
    let parent = input.parentElement;
    //get the small tag of the parent element
    let message = parent.querySelector("small");
    //hide the error message
    message.style.visibility = "hidden";
    //remove the error styling of the red border etc
    parent.classList.remove("error");
    //add the green border to the parent
    parent.classList.add("success");
}

/*helper function to assign error features
to input argument
input will be name, email or notes
alertMsg will depend on what error has occurred
regarding form validation*/
function onError(input, alertMsg){
    //target the parent element and store as variable
    let parent = input.parentElement;
    //get the small tag of the parent element
    let message = parent.querySelector("small");
    //show the error message
    message.style.visibility = "visible";
    //set the inner text to reflect the error
    //based on the message
    message.innerText = alertMsg;
    //add the error styling of the red border etc
    parent.classList.add("error");
    //remove the green border from the border
    parent.classList.remove("success");
}

//function to check if email is valid using regex
const isValidEmail = (email) => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  };


/** 
 * Thanks very much for the opportunity to attempt the test, it's much appreciated and 
 * was really enjoyable. I really wanted to go for the bonus points features but 
 * unfortunately with trying to juggling university projects I didn't have the time
 * to research further. However, it sounds really interesting and will definitely be
 * something I will work on as they will be very useful skills to bring to the workplace.
 * 
 * I'm really keen to get stuck into learning React once I finish my degree next week and
 * to explore unit testing for JavaScript, as I've only has experience with php and java
 * unit testing.
 * 
 * Thanks again, Chris :)
*/


